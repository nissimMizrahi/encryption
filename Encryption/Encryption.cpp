#include "Encryption.h"

Encryption::Buffer Encryption::generateKey(size_t size)
{
	CryptoPP::AutoSeededRandomPool rng;
	Buffer ret;
	std::fill_n(std::back_inserter(ret), size, 0);

	rng.GenerateBlock(ret.data(), size);
	return ret;
}

Encryption::Buffer Encryption::AES_Encrypt(const Buffer& data, const Buffer& key, const Buffer& iv)
{
	CryptoPP::AES::Encryption aesEncryption(key.data(), key.size());
	CryptoPP::CBC_Mode_ExternalCipher::Encryption cbcEncryption(aesEncryption, iv.data());

	Buffer ret;
	CryptoPP::StreamTransformationFilter stfEncryptor(cbcEncryption, new CryptoPP::VectorSink(ret));
	stfEncryptor.Put(reinterpret_cast<const unsigned char*>(data.data()), data.size());
	stfEncryptor.MessageEnd();

	return ret;
}

Encryption::Buffer Encryption::AES_Decrypt(const Buffer& data, const Buffer& key, const Buffer& iv)
{
	CryptoPP::AES::Decryption aesDecryption(key.data(), key.size());
	CryptoPP::CBC_Mode_ExternalCipher::Decryption cbcDecryption(aesDecryption, iv.data());

	Buffer ret;
	CryptoPP::StreamTransformationFilter stfDecryptor(cbcDecryption, new CryptoPP::VectorSink(ret));
	stfDecryptor.Put(reinterpret_cast<const unsigned char*>(data.data()), data.size());
	stfDecryptor.MessageEnd();

	return ret;
}

void Encryption::generatePair(Buffer& public_key, Buffer& private_key)
{
	CryptoPP::AutoSeededRandomPool rng;
	CryptoPP::InvertibleRSAFunction params;
	
	params.Initialize(rng, 1024);

	CryptoPP::RSA::PublicKey pub(params);
	CryptoPP::RSA::PrivateKey pri(params);

	CryptoPP::Base64Encoder privkeysink(new CryptoPP::VectorSink(private_key));
	pri.DEREncode(privkeysink);
	privkeysink.MessageEnd();

	CryptoPP::Base64Encoder pubkeysink(new CryptoPP::VectorSink(public_key));
	pub.DEREncode(pubkeysink);
	pubkeysink.MessageEnd();

}

Encryption::Buffer Encryption::RSA_Encrypt(const Buffer& data, const Buffer& public_key)
{
	CryptoPP::AutoSeededRandomPool rng;
	CryptoPP::VectorSource key_src(public_key, true, new CryptoPP::Base64Decoder);

	CryptoPP::RSA::PublicKey key;
	key.Load(key_src);

	CryptoPP::RSAES_OAEP_SHA_Encryptor enc(key);
	
	Buffer ret;
	CryptoPP::VectorSource(
		data,
		true,
		new CryptoPP::PK_EncryptorFilter(rng, enc, new CryptoPP::VectorSink(ret))
	);

	return ret;
}

Encryption::Buffer Encryption::RSA_Decrypt(const Buffer& data, const Buffer& private_key)
{
	CryptoPP::AutoSeededRandomPool rng;
	CryptoPP::VectorSource key_src(private_key, true, new CryptoPP::Base64Decoder);

	CryptoPP::RSA::PrivateKey key;
	key.Load(key_src);

	CryptoPP::RSAES_OAEP_SHA_Decryptor dec(key);

	Buffer ret;
	CryptoPP::VectorSource(
		data,
		true,
		new CryptoPP::PK_DecryptorFilter(rng, dec, new CryptoPP::VectorSink(ret))
	);

	return ret;
}

Encryption::Buffer Encryption::sha256(const Buffer& data)
{
	byte digest[CryptoPP::SHA256::DIGESTSIZE];

	CryptoPP::SHA256().CalculateDigest(digest, data.data(), data.size());

	Buffer ret;
	std::fill_n(std::back_inserter(ret), CryptoPP::SHA256::DIGESTSIZE * 2, 0);

	CryptoPP::HexEncoder hexEncoder;
	hexEncoder.Put(digest, CryptoPP::SHA256::DIGESTSIZE);
	hexEncoder.Get(ret.data(), CryptoPP::SHA256::DIGESTSIZE * 2);

	return ret;
}

#include "Encryption/Encryption.h"

#include <iostream>

int main()
{
	//auto key = Encryption::generateKey(CryptoPP::AES::DEFAULT_KEYLENGTH);
	//auto enc = Encryption::AES_Encrypt({ 'a', 'b', 'c' }, key);
	//auto dec = Encryption::AES_Decrypt(enc, key);

	Encryption::Buffer pri, pub;
	Encryption::generatePair(pub, pri);

	auto enc = Encryption::RSA_Encrypt({ 'a', 'b', 'c' }, pub);
	auto dec = Encryption::RSA_Decrypt(enc, pri);
	auto shad = Encryption::sha256(dec);
	std::cout << std::string(shad.begin(), shad.end());
	return 0;
}
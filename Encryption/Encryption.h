#pragma once
#include <string>
#include <vector>
#include <algorithm>

#include "cryptopp/aes.h"
#include "cryptopp/rsa.h"
#include "cryptopp/filters.h"
#include "cryptopp/modes.h"
#include "cryptopp/osrng.h"
#include "cryptopp/base64.h"
#include "cryptopp/hex.h"

#define DEFAULT_IV {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

class Encryption
{
	Encryption();
	~Encryption();

public:

	typedef unsigned char byte;
	typedef std::vector<byte> Buffer;

	static Buffer generateKey(size_t size);
	static Buffer AES_Encrypt(const Buffer& data, const Buffer& key, const Buffer& iv = DEFAULT_IV);
	static Buffer AES_Decrypt(const Buffer& data, const Buffer& key, const Buffer& iv = DEFAULT_IV);

	static void generatePair(Buffer& public_key, Buffer& private_key);
	static Buffer RSA_Encrypt(const Buffer& data, const Buffer& public_key);
	static Buffer RSA_Decrypt(const Buffer& data, const Buffer& private_key);
	static Buffer sha256(const Buffer& data);

};

